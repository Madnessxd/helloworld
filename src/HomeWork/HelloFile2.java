package HomeWork;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;

public class HelloFile2 {

    public static void main(String[] args) {
        BufferedReader br = null;
        try {
            File file = new File("HelloWorld2.txt");

            if (!file.exists())
                file.createNewFile();

            PrintWriter pw = new PrintWriter(file);
            pw.println("Hello ");
            pw.println("World!!!");
            pw.close();

            br = new BufferedReader(new FileReader("HelloWorld2.txt"));
            String line;
            while ((line = br.readLine()) != null) {
                System.out.print(line);
            }
        } catch (IOException e) {
            System.out.print("Error: " + e);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                System.out.print("Error: " + e);
            }
        }
    }
}


