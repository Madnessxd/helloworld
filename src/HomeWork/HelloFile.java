package HomeWork;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloFile {

    public static void main(String[] args) {
        try {
            File file = new File("HelloWorld.txt");

            if(!file.exists())
                file.createNewFile();

                PrintWriter pw = new PrintWriter(file);
                pw.println("Hello World!!!");
                pw.close();
        }
            catch(IOException e) {
                System.out.print("Error: " +e);
            }
        }
    }

